package za.co.danielcarr.models;

public enum TaxType {
    BOOK(0),
    FOOD(0),
    MEDICAL(0),
    OTHER(10);

    private final int rate;

    TaxType(int rate) {
        this.rate = rate;
    }

    public int rate() {
        return rate;
    }
}
