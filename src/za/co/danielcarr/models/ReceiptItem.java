package za.co.danielcarr.models;

import za.co.danielcarr.rules.TaxRules;

public class ReceiptItem {
    private String name;
    private Integer price;
    private TaxType taxType;
    private Boolean imported;

    ReceiptItem(String name, Integer price) {
        new ReceiptItem(name, price, TaxType.OTHER, false);
    }

    ReceiptItem(String name, Integer price, TaxType type, Boolean imported) {
        this.name = name;
        this.price = price;
        this.taxType = type;
        this.imported = imported;
    }

    public String name() {
        return name;
    }

    public Integer shelfPrice() {
        return price;
    }

    public Integer truePrice() {
        return roundUp(Math.round(price * taxRate()), TaxRules.priceToRoundTo());
    }

    private float taxRate() {
        float taxRate = 1 + TaxRules.rateForType(taxType);
        if (imported) {
            taxRate += TaxRules.importDuty();
        }
        return taxRate;
    }

    private Integer roundUp(Integer value, Integer toNearest) {
        Integer roundedValue = value;
        Integer difference = value % toNearest;
        if (difference > 0) {
            roundedValue += toNearest - difference;
        }
        return roundedValue;
    }
}
