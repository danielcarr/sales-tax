package za.co.danielcarr.models;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Vector;

public class Receipt {
    private Vector<ReceiptItem> items;
    private HashMap<String, Integer> quantities;

    public void addItem(String name, Integer price, TaxType taxType, Boolean imported) {
        addItem(new ReceiptItem(name, price, taxType, imported));
    }

    public void addItem(String name, Integer price) {
        addItem(new ReceiptItem(name, price));
    }

    public void addItem(ReceiptItem item) {
        addItems(1, item);
    }

    public void addItems(int quantity, String name, Integer price, TaxType taxType, Boolean imported) {
        addItems(quantity, new ReceiptItem(name, price, taxType, imported));
    }

    public void addItems(int quantity, String name, Integer price) {
        addItems(quantity, new ReceiptItem(name, price));
    }

    public void addItems(int quantity, ReceiptItem item) {
        if (items == null) {
            items = new Vector<ReceiptItem>();
            quantities = new HashMap<String, Integer>();
        }
        String itemName = item.name();
        Integer currentQuantity = quantityOfItem(itemName);
        if (currentQuantity > 0) {
            quantities.put(itemName, currentQuantity + quantity);
        } else {
            items.add(item);
            quantities.put(itemName, quantity);
        }
    }

    public Integer quantityOfItem(String name) {
        if (quantities == null) {
            return 0;
        }
        Integer quantity = quantities.get(name);
        return quantity == null ? 0 : quantity;
    }

    public Integer labelCost() {
        return cost(false);
    }

    public Integer trueCost() {
        return cost(true);
    }

    public Integer salesTax() {
        return trueCost() - labelCost();
    }

    public String stringForLine(Integer index) {
        ReceiptItem item = items.get(index);
        String name = item.name();
        int quantity = quantityOfItem(name);
        Integer price = item.truePrice();
        Integer cents = price % 100;
        Integer rands = price / 100;
        return String.format("%d %s: %d.%d", quantity, name, rands, cents);
    }

    public ReceiptLine entryAtIndex(Integer index) throws IndexOutOfBoundsException {
        if (items == null || index > items.size()) {
            throw new IndexOutOfBoundsException();
        }
        ReceiptItem item = items.get(index);
        String name = item.name();
        Integer quantity = quantityOfItem(name);
        Integer cost = item.truePrice();
        return new ReceiptLine(quantity, name, cost);
    }

    public int lines() {
        return items.size();
    }

    public int totalItems() {
        int sum = 0;
        for (int quantity : quantities.values()) {
            sum += quantity;
        }
        return sum;
    }

    private Integer cost(boolean withTax) {
        Integer totalPrice = 0;
        try {
            Method calculatePrice = withTax ?
                ReceiptItem.class.getMethod("truePrice") : ReceiptItem.class.getMethod("shelfPrice");
            for (ReceiptItem item : items) {
                String itemName = item.name();
                Integer quantity = quantityOfItem(itemName);
                Integer subtotal = quantity * (Integer)calculatePrice.invoke(item);
                totalPrice += subtotal;
            }
        }
        // None of these should ever happen, and if they do, something has gone very wrong and
        // there's no value for this method that makes sense, so there's nothing to do but bail.
        catch (NoSuchMethodException x) {
            System.exit(-1);
        } catch (InvocationTargetException x) {
            System.exit(-2);
        } catch (IllegalAccessException x) {
            System.exit(-3);
        }
        return totalPrice;
    }

    public class ReceiptLine {
        private final Integer quantity;
        private final String name;
        private final Integer cost;

        public ReceiptLine(Integer quantity, String name, Integer cost) {
            this.quantity = quantity;
            this.name = name;
            this.cost = cost;
        }

        public Integer quantity() {
            return quantity;
        }

        public String name() {
            return name;
        }

        public Integer cost() {
            return cost;
        }
    }
}
