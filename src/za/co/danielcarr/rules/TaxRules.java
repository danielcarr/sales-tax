package za.co.danielcarr.rules;

import za.co.danielcarr.models.TaxType;

public class TaxRules {
    private static final int importDuty = 5;
    private static final int priceToRoundTo = 5;

    public static float rateForType(TaxType type) {
        return floatForRate(type.rate());
    }

    public static float importDuty() {
        return floatForRate(importDuty);
    }

    public static int priceToRoundTo() {
        return priceToRoundTo;
    }

    private static float floatForRate(int percentage) {
        return percentage / 100F;
    }
}
