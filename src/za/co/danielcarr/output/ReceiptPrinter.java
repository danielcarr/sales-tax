package za.co.danielcarr.output;

import za.co.danielcarr.models.Receipt;
import za.co.danielcarr.presenters.PriceFormatter;

public class ReceiptPrinter {
    public static void printReceipt(Receipt receipt) {
        for (int i = 0; i < receipt.lines(); ++i) {
            Receipt.ReceiptLine line = receipt.entryAtIndex(i);
            String cost = PriceFormatter.currencyStringForInteger(line.cost());
            System.out.printf("%d %s: %s\n", line.quantity(), line.name(), cost);
        }
        System.out.printf("Sales taxes: %s\n", PriceFormatter.currencyStringForInteger(receipt.salesTax()));
        System.out.print("Total: ");
        System.out.println(PriceFormatter.currencyStringForInteger(receipt.trueCost()));
    }
}
