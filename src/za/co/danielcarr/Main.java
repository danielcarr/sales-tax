package za.co.danielcarr;

import za.co.danielcarr.models.Receipt;
import za.co.danielcarr.models.TaxType;
import za.co.danielcarr.output.ReceiptPrinter;

public class Main {
    public static void main(String[] args) {
        Receipt[] baskets = createBaskets();
        int numberOfBaskets = baskets.length;
        String calculating = String.format("Calculating receipts for %d baskets", numberOfBaskets);
        printEphemerally(calculating, 2000);
        for (int r = 1; r <= numberOfBaskets; ++ r) {
            System.out.println();
            System.out.println(String.format("Output %d:", r));
            ReceiptPrinter.printReceipt(baskets[r - 1]);
        }
    }

    private static Receipt[] createBaskets() {
        Receipt[] baskets = new Receipt[3];
        printEphemerally("Filling baskets...", 800);
        printEphemerally("Basket 1", 650);
        baskets[0] = new Receipt();
        printEphemerally("Adding a book at R12.49");
        baskets[0].addItem("book", 1249, TaxType.BOOK, false);
        printEphemerally("Adding a CD at R14.99");
        baskets[0].addItem("music CD", 1499, TaxType.OTHER, false);
        printEphemerally("Adding a chocolate bar at 85 cents");
        baskets[0].addItem("chocolate bar", 85, TaxType.FOOD, false);
        printEphemerally("Basket 2", 650);
        baskets[1] = new Receipt();
        printEphemerally("Adding an imported box of chocolates for R10.00");
        baskets[1].addItem("imported box of chocolates", 1000, TaxType.FOOD, true);
        printEphemerally("Adding imported perfume for R47.50");
        baskets[1].addItem("imported bottle of perfume", 4750, TaxType.OTHER, true);
        printEphemerally("Basket 3", 650);
        baskets[2] = new Receipt();
        printEphemerally("Adding imported perfume for R27.99");
        baskets[2].addItem("imported bottle of perfume", 2799, TaxType.OTHER, true);
        printEphemerally("Adding local perfume for R18.99");
        baskets[2].addItem("bottle of perfume", 1899, TaxType.OTHER, false);
        printEphemerally("Adding headache pills for R9.75");
        baskets[2].addItem("packet of headache pills", 975, TaxType.MEDICAL, false);
        printEphemerally("Adding a box of imported chocolates for R11.25");
        baskets[2].addItem("box of imported chocolates", 1125, TaxType.FOOD, true);
        printEphemerally("Finished creating baskets", 800);
        return baskets;
    }

    private static void printEphemerally(String text) {
        printEphemerally(text, 500);
    }

    private static void printEphemerally(String text, int timeToSee) {
        System.err.print(text);
        try {
            Thread.sleep(timeToSee);
        } catch (InterruptedException x) {
            // Ignore this error; it isn't important here
        }
        System.err.print("\r\u001b[2K");
    }
}
