package za.co.danielcarr.presenters;

public class PriceFormatter {

    private static final String CURRENCY_FORMAT = "%d.%02d";

    public static Integer integerForCurrency(Float decimalValue) {
        return new Integer(Math.round(100 * decimalValue));
    }

//    public static Integer internalPriceForCurrency(String currencyString) {
//        if (! currencyString.matches(".?[0-9]*(?:[.,:-](?:[0-9][0-9].?))?")) {
//            throw new InvalidArgumentException();
//        }
//        String numbersOnlyCurrencyString = currencyString.replaceAll("[^0-9,.:-]", "");
//        String wellFormedCurrency = numbersOnlyCurrencyString.
//    }

    public static String currencyStringForInteger(Integer priceInCents) {
        int cents = priceInCents % 100;
        int rands = (priceInCents - cents) / 100;
        return String.format(CURRENCY_FORMAT, rands, cents);
    }
}